import json
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from collections import defaultdict


app = Flask(__name__)
# CORS(app, resources={r"/*": {"origins": "file:///Users/sebastian/Documents/Studia/GIS/kebab/OpenLayersMap.html"}})
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///reviews.db'
db = SQLAlchemy(app)

class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    review_text = db.Column(db.String(500))
    index = db.Column(db.Integer)

@app.route('/map')
def map():
    return app.send_static_file('OpenLayersMap.html')

@app.route('/')
def home():

    reviews = Review.query.all()
    reviews_data = [{'id': review.id, 'review_text': review.review_text, 'index': review.index} for review in reviews]
    return reviews_data



# @app.route('/')
# def reviews():
    # all_reviews = Review.query.all()  # Pobranie wszystkich recenzji
    # reviews_str = '<br>'.join([review.review_text for review in all_reviews])
    # return f"Lista recenzji: <br>{reviews_str}"

@app.route('/api/reviews')
def get_reviews():
    reviews = Review.query.all()
    reviews_data = defaultdict(list)  # Używamy defaultdict do łatwego grupowania
    for review in reviews:
        index = review.index
        reviews_data[index].append(review.review_text)

    # Konwersja słownika na listę list
    sorted_reviews = [reviews for _, reviews in sorted(reviews_data.items())]
    return jsonify(sorted_reviews)

def load_reviews(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        for index, reviews in data.items():
            for review_text in reviews:
                new_review = Review(review_text=review_text, index=index)
                db.session.add(new_review)
        db.session.commit()

def add_review(review_text, index):
    new_review = Review(review_text=review_text, index=index)
    db.session.add(new_review)
    db.session.commit()

@app.route('/add_review', methods=['POST'])
def add_review():
    data = request.json
    new_review = Review(review_text=data['review'], index=data['index'])
    db.session.add(new_review)
    db.session.commit() 
    return 

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
        app.run(debug=True)
        # load_reviews("review.json")
